﻿Set-StrictMode -Version 'Latest'


Function Start-PingMachine {
    PARAM(
        [Parameter(Mandatory = $False)]
        [int] $Timeout = 2000,
        # -----------------------------
        [Parameter(Mandatory = $False)]
        [int] $Interval = 1000,
        # -----------------------------
        [Parameter(Mandatory)]
        [String] $Address
    )
    Write-Debug "Begin: $($MyInvocation.MyCommand.Name)"

    $Pinger = New-Object System.Net.NetworkInformation.Ping
    While ( $True ) {
        
        [string] $Message = $Null
        [bool] $IsConnected = $False

        # Build message
        Try { 
            $Response = $Pinger.Send($Address,$Timeout)
            $IsConnected = $Response.Status -eq 'Success'

            $Message = $(
                $Response | Format-Table -AutoSize `
                                         -HideTableHeaders `
                                         -Property @(
                    'Status', 'Address', 'RoundtripTime'
                ) | Out-string
            ).Trim()
        } Catch {
            $Message = $_.Exception.Message
            $IsConnected = $False
        }
        
        # Pick a color
        $ColorSplat = If ( $IsConnected ) {
            @{
                BackgroundColor = 'DarkGreen'
                ForegroundColor = 'White'
            }
        } Else {
            @{
                BackgroundColor = 'DarkRed'
                ForegroundColor = 'White'
            }
        }

        $Message.Trim() | Write-Host @ColorSplat
        Start-Sleep -Milliseconds $Interval
    }

    Write-Debug "End: $($MyInvocation.MyCommand.Name)"
}